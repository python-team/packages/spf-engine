Source: spf-engine
Section: mail
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Scott Kitterman <scott@kitterman.com>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
Build-Depends-Indep: python3, flit (>= 3.8), pybuild-plugin-pyproject
Standards-Version: 4.7.0
Homepage: https://launchpad.net/spf-engine
Vcs-Git: https://salsa.debian.org/python-team/packages/spf-engine.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/spf-engine
Rules-Requires-Root: no

Package: postfix-policyd-spf-python
Architecture: all
Depends:
 adduser,
 postfix,
 python3-spf-engine (= ${binary:Version}),
 ${python3:Depends},
 ${misc:Depends},
Description: Postfix policy server for SPF checking
 postfix-policyd-spf-python is a full featured Postfix policy engine for
 SPF checking. It includes a variety of whitelisting mechanisms and policy
 options to enable it to support the widest variety of system requirements.
 It is implemented in pure Python and uses the python-spf module.  The SPF
 web site is http://www.openspf.net/.  It has been updated to support RFC
 7208.

Package: pyspf-milter
Architecture: all
Pre-Depends: ${misc:Pre-Depends}
Depends:
 adduser,
 postfix|sendmail,
 python3-milter,
 python3-spf-engine (= ${binary:Version}),
 ${python3:Depends},
 ${misc:Depends},
Description: Modern milter for SPF checking
 pyspf-milter is a full featured milter for SPF checking. It includes a
 variety of whitelisting mechanisms and policy options to enable it to
 support the widest variety of system requirements.  It is implemented in pure
 Python and uses the python-spf module.  The SPF web site is
 http://www.openspf.net/.  It has been updated to support RFC 7208.

Package: python3-spf-engine
Section: python
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: Sender Policy Framework (SPF) processing module
 spf-engine provides the core processing for postfix-policyd-spf-python and
 pyspf-milter.  It is not meant to be installed independently.
